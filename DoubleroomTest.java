import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleroomTest {

    @Test
    public void userData() {
        Doubleroom userDetails = new Doubleroom("luna","9236541251","male","lisa","123456789","female");
        assertEquals("luna",userDetails.name);
        assertEquals("9236541251",userDetails.contact);
        assertEquals("male",userDetails.gender);
        assertEquals("lisa",userDetails.name2);
        assertEquals("123456789",userDetails.contact2);
        assertEquals("female",userDetails.gender2);
    }

    @Test
    public void checkNotNull(){
        Doubleroom notNull = new Doubleroom("luna","9236541251","male","lisa","123456789","female");
        assertNotNull(notNull.name);
        assertNotNull(notNull.contact);
        assertNotNull(notNull.gender);
        assertNotNull(notNull.name2);
        assertNotNull(notNull.contact2);
        assertNotNull(notNull.gender2);
    }

    @Test
    public void dataLength(){
        Doubleroom lengthMatch = new Doubleroom("luna","9236541251","male","lisa","123456789","female");
        assertTrue("luna".length() == lengthMatch.name.length());
        assertTrue("9236541251".length() == lengthMatch.contact.length());
        assertTrue("lisa".length() == lengthMatch.name2.length());
        assertTrue("123456789".length() == lengthMatch.contact2.length());
    }

    @Test
    public void checkForNull(){
        Doubleroom checkNull = new Doubleroom("luna","9236541251","male","lisa","123456789","female");
        assertNull(null);
    }

    @Test
    public void notSimilar(){
        Doubleroom similarData = new Doubleroom("luna","9236541251","male","lisa","123456789","female");
        assertFalse("lunaa" == similarData.name);
        assertFalse("56464" == similarData.contact);
        assertFalse("female " == similarData.gender);
        assertFalse("mona" == similarData.name2);
        assertFalse("569694" == similarData.contact2);
        assertFalse("male " == similarData.gender2);

    }

    @AfterClass
    public static void  printData(){
        System.out.println("DoubleRoom testcase completed");
    }

}