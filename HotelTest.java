import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class HotelTest {

    @Test
    public void checkRoom(){
        Hotel .hotel_ob.deluxe_doublerrom[0] = new Doubleroom("luna","3698741258","female","lisa","123456789","female");
        String expected = "luna";
        assertEquals(expected,Hotel .hotel_ob.deluxe_doublerrom[0].name);

    }

    @Test
    public void checkNotNull(){
        Hotel.hotel_ob.luxury_singleerrom[1] = new Singleroom("tony","58569745","male");
        assertNotNull( Hotel.hotel_ob.luxury_singleerrom[1].name);
    }

    @Test
    public void dataLength(){
        Hotel.hotel_ob.luxury_singleerrom[1] = new Singleroom("tony","58569745","male");
        int excepted = Hotel.hotel_ob.luxury_singleerrom[1].contact.length();
        assertTrue(excepted == Hotel.hotel_ob.luxury_singleerrom[1].contact.length());
    }

    @Test
    public void checkForNull(){
        Hotel.hotel_ob.luxury_singleerrom[1] = new Singleroom("peter","58569745","male");
        assertNull(null);

    }

    @AfterClass
    public static void  printData(){

        System.out.println("Hotel testcase completed");
    }

}