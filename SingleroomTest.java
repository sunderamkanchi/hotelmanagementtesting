import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class SingleroomTest {

    @Test
    public void userData(){
        Singleroom userDetails = new Singleroom("luna","9236541251","male");
        assertEquals("luna",userDetails.name);
        assertEquals("9236541251",userDetails.contact);
        assertEquals("male",userDetails.gender);
    }

    @Test
    public void checkNotNull(){
        Singleroom notNull = new  Singleroom("luna","9236541251","male");
        assertNotNull(notNull.name);
        assertNotNull(notNull.contact);
        assertNotNull(notNull.gender);
    }

    @Test
    public void dataLength(){
        Singleroom lengthMatch = new Singleroom("luna","9236541251","male");
        assertTrue("luna".length() == lengthMatch.name.length());
        assertTrue("9236541251".length() == lengthMatch.contact.length());
    }

    @Test
    public void checkForNull(){
        Singleroom checkNull = new Singleroom("luna","9236541251","male");
        assertNull(null);
    }

    @Test
    public void notSimilar(){
        Singleroom similarData = new Singleroom("luna","9236541251","male");
        assertFalse("lunaa" == similarData.name);
        assertFalse("56464" == similarData.contact);
        assertFalse("female " == similarData.gender);

    }

    @AfterClass
    public static void  printData(){
        System.out.println("SingleRoom testcase completed");
    }

}