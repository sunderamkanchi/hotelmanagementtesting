import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class FoodTest {

    @Test
    public void testFoodPrice(){
        Food foodType = new Food(2, 2);
        int expected = 120;
        assertEquals(expected,foodType.price, 0.0);
    }

    @Test
    public void testFoodQuantity(){
        Food foodQuantity = new Food(2, 2);
        int expected = 2;
        assertEquals(expected,foodQuantity.quantity);
    }

    @Test
    public void checkNotNull(){
        Food foodType = new Food(2, 2);
        assertNotNull(foodType.price);
    }

    @Test
    public void CheckForNull(){
        Food foodType = new Food(2, 2);
        assertNull(null);
    }

    @Test
    public void testFoodQuantityEqual(){
        Food foodQuantity = new Food(2, 2);
        int expected = 2;
        assertTrue(expected == foodQuantity.quantity);
    }

    @AfterClass
    public static void printData(){
        System.out.println("Food Testcase completed");
    }

}