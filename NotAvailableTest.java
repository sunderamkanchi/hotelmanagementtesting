import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class NotAvailableTest {

    @Test
    public void checkNotAvailable(){
        NotAvailable  notAvailable = new NotAvailable();
         String expected = "Not Available !";
         assertEquals(expected,notAvailable.toString());
    }

    @Test
    public void checkNotNull(){
        NotAvailable notNull = new NotAvailable();
        assertNotNull(notNull.toString());
    }

    @Test
    public void checkNull(){
        NotAvailable checkNull = new NotAvailable();
        assertNull(null);

    }

    @AfterClass
    public static void printData(){
        System.out.println("NotAvailable Testcase completed");
    }
}