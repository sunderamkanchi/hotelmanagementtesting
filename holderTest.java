import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class holderTest {

    @Test
    public void checkRoomCount(){
        holder checkRoom = new holder();
        int expected = 20;
        assertEquals(expected,checkRoom.deluxe_singleerrom.length);
    }

    @Test
    public void checkRoomCount2(){
        holder checkRoom = new holder();
        int expected = 20;
        assertEquals(expected,checkRoom.deluxe_doublerrom.length);
    }

    @Test
    public void checkRoomCount3(){
        holder checkRoom = new holder();
        int expected = 10;
        assertEquals(expected,checkRoom.luxury_singleerrom.length);
    }

    @Test
    public void checkRoomCount4(){
        holder checkRoom = new holder();
        int expected = 10;
        assertEquals(expected,checkRoom.luxury_doublerrom.length);
    }

    @AfterClass
    public static void printData(){
        System.out.println("NotAvailable Testcase completed");
    }

}